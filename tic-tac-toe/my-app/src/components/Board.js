import '../index.css';
import React from 'react'
import Square from './Square'
import Status, {calculateStatus} from './Status'


const renderSquare = (i, squares, setSquares, xIsNext, setXIsNext, winner) => (
  <Square 
    value = {squares[i]}
    onClick={
      () => {
        if (winner === null && squares[i] === null) {
          squares[i] = xIsNext ? 'X' : 'O';
          setSquares(squares);
          setXIsNext(!xIsNext);
        }
      }
    }
  />
);

const renderStatus = (winner, xIsNext) => (
  <Status
    status = {calculateStatus(winner, xIsNext)}
  />
);
  
const Board = ({winner, squares, setSquares, xIsNext, setXIsNext}) => (
  <div>
    {renderStatus(winner, xIsNext)}
    <div className="board-row">
      {renderSquare(0, squares, setSquares, xIsNext, setXIsNext, winner)}
      {renderSquare(1, squares, setSquares, xIsNext, setXIsNext, winner)}
      {renderSquare(2, squares, setSquares, xIsNext, setXIsNext, winner)}
    </div>
    <div className="board-row">
      {renderSquare(3, squares, setSquares, xIsNext, setXIsNext, winner)}
      {renderSquare(4, squares, setSquares, xIsNext, setXIsNext, winner)}
      {renderSquare(5, squares, setSquares, xIsNext, setXIsNext, winner)}
    </div>
    <div className="board-row">
      {renderSquare(6, squares, setSquares, xIsNext, setXIsNext, winner)}
      {renderSquare(7, squares, setSquares, xIsNext, setXIsNext, winner)}
      {renderSquare(8, squares, setSquares, xIsNext, setXIsNext, winner)}
    </div>
  </div>
);

export default Board