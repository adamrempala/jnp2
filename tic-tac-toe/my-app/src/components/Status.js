import '../index.css';
import React from 'react'

export const Status = ({status}) => (
    <div className="status">{status}</div>
  );

export const calculateStatus = (winner, xIsNext) => {
  let status;
  
  if (winner !== null) {
    status = `Winner: ${winner}`;
  } else {
    status = `Next player: ${xIsNext ? 'X' : 'O'}`;
  }

  return status;

}
  
export default Status;