import '../index.css';
import React from 'react'

export const Square = ({value, onClick}) => (
    <button
      className="square"
      onClick={onClick}
    >
      {value}
    </button>
  );

export default Square;