import ReactDOM from 'react-dom';
import './index.css';
import React from 'react'
import Game from './components/Game'

  // ========================================
  
  ReactDOM.render(
    <Game />,
    document.getElementById('root')
  );


  